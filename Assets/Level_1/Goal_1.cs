    using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Goal_1 : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        PlayerMovement component = other.gameObject.GetComponent<PlayerMovement>();
        if (component != null)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        }
    }
}