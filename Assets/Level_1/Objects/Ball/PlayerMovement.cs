using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PlayerMovement : MonoBehaviour
{
    public float threshold = -50f;
    public TMP_Text countText;
    public TMP_Text winText;
    Rigidbody rb;
    private int count;
    [SerializeField] float baseMovementSpeed = 6f;
    private float difficulty = 1f;

    // Start is called before the first frame update
    void Start()
    {
        count = 0;
        SetCountText();
        rb = GetComponent<Rigidbody>();
        winText.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (transform.position.y < threshold)
        {
            difficulty-=1f;
        }
        if (Input.GetKeyDown("space"))
        {
            GetComponent<Rigidbody>().velocity = new Vector3(0, 5, 0);
        }
        float horizontalInput = Input.GetAxis("Horizontal");
        float verticalInput = Input.GetAxis("Vertical");

        // Calculer la vitesse de mouvement en fonction de la difficulté
        float movementSpeed = baseMovementSpeed + (difficulty * 2f);

        rb.velocity = new Vector3(horizontalInput * movementSpeed, rb.velocity.y, verticalInput * movementSpeed);
    }

    private void OnTriggerEnter(Collider other)
    {
        count += 1;
        other.gameObject.SetActive(false);
        SetCountText();

        // Augmenter la difficulté en fonction du score
        if (count % 2 == 0)
        {
            difficulty += 1f;
        }
    }

    void SetCountText()
    {
        countText.text = "Score: " + count.ToString();
        if (count > 5)
        {
            winText.gameObject.SetActive(true);
        }
    }
}