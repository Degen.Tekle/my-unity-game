using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Goal_2 : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        PlayController_1 component = other.gameObject.GetComponent<PlayController_1>();
        if (component != null)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        }
    }
}
