using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerLife : MonoBehaviour
{
    private float count = 0f;
    public float threshold = -50f;
    private Vector3 initialPosition;
    
    void Start()
    {
        // Enregistrer la position initiale du GameObject
        initialPosition = transform.position;
    }
    
    void Update()
    {
        if (transform.position.y <= threshold)
        {
            count+=1f;
        }
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Enemy Body") && count < 2f )
        {
            Die();
        }
        if (collision.gameObject.CompareTag("Enemy Body") && count >= 2f )
        {
            transform.position = initialPosition;
        }
        
        
    }

    void Die()
    {
        GetComponent<MeshRenderer>().enabled = false;
        GetComponent<Rigidbody>().isKinematic = true;
        GetComponent<PlayController_1>().enabled = false;
        ReloadLevel();
    }

    void ReloadLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
