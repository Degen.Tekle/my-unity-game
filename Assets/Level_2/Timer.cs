using System.Collections; 
using System.Collections.Generic; 
using UnityEngine; 
public class Timer : MonoBehaviour 
{ 
    public float timerVar; 

// Update is called once per frame
    void Update()
    {
        if (timerVar > 0)
        {
            timerVar -= Time.deltaTime;
        }
        else
        {
            Destroy(gameObject);
        }
    } 
}