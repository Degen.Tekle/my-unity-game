using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public TMP_Text countText;
    public TMP_Text winText;
    Rigidbody rb;
    private int count;
    [SerializeField] float movementSpeed = 6f;
    // Start is called before the first frame update
    void Start()
    {
        count = 0;
        SetCountText();
        rb = GetComponent<Rigidbody>();
        winText.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        float horizontalInput = Input.GetAxis("Horizontal");
        float verticalInput = Input.GetAxis("Vertical");

        rb.velocity = new Vector3(horizontalInput * movementSpeed, rb.velocity.y, verticalInput * movementSpeed);
        
    }

    private void OnTriggerEnter(Collider other)
    {
        count += 1;
        other.gameObject.SetActive(false);
        SetCountText();
    }

    void SetCountText()
    {
        countText.text = "Score: " + count.ToString();
        if (count > 4)
        {
            winText.gameObject.SetActive(true);
        }
    }
}