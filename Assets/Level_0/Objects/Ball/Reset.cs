using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Reset : MonoBehaviour
{
    private Vector3 initialPosition;
    public float threshold = -50f;
    
    void Start()
    {
        // Enregistrer la position initiale du GameObject
        initialPosition = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        if (transform.position.y < threshold)
        {
            transform.position = initialPosition;
        }
    }
}