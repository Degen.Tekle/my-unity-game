using UnityEngine;
using TMPro;

public class Consigne : MonoBehaviour
{
    public TextMeshProUGUI consigneText;

    void Start()
    {
        consigneText.enabled = true;
    }
}